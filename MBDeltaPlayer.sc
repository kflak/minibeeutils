MBDeltaPlayer { 

    classvar <>numSpeakers = 2;

    var <>minibeeID;
    var <>bufs;
    var <>speedlim;
    var <>threshold;
    var <>minAmp;
    var <>maxAmp;
    var <>out;
    var <>targetGroup;
    // var <>startPos;
    // var <>endPos;
    var <>speed;
    var <>reverseProbability;
    var <>revTime;
    var <>revLevelRange;
    var <>pitchRange;
    var <>attackScale;
    var <>releaseScale;
    var <>durScale;
    var <>maxDuration;
    var <>grainFreqRange;
    var <>rateRange;
    var <>grainSizeRange;
    var <>panRange; 
    var <>locut;
    var <>hishelffreq;
    var <>hishelfdb;
    var <>hicut;
    var <>peakfreq;
    var <>peakrq;
    var <>peakdb;
    var group;
    var kGroup;
    var instrGroup;
    var routeGroup;
    var fxGroup;
    var <trigs;
    var <route;
    var <reverb;
    var <>revBus;
    var aBus;
    var outGroup;
    var server;

    *new { arg minibeeID=#[9, 10, 11, 12, 13, 14, 15, 16], bufs, speedlim=0.5, threshold=0.1, minAmp=0.1, maxAmp=0.6, out=0, targetGroup, speed=1, reverseProbability=0, revTime=2, revLevelRange=#[0.001, 0.01], pitchRange=#[1, 1], attackScale=0.4, releaseScale=0.4, durScale=1, maxDuration=60, grainFreqRange=#[10, 10], rateRange=#[1, 1], grainSizeRange=#[0.5, 0.5], panRange=#[-1, 1], locut=20, hishelffreq=200, hishelfdb= -6, hicut=20000, peakfreq=600, peakrq=1, peakdb=0;
    ^super.newCopyArgs(minibeeID, bufs, speedlim, threshold, minAmp, maxAmp, out, targetGroup, speed, reverseProbability, revTime, revLevelRange, pitchRange, attackScale, releaseScale, durScale, maxDuration, grainFreqRange, rateRange, grainSizeRange, panRange, locut, hishelffreq, hishelfdb, hicut, peakfreq, peakrq, peakdb).init;
}

init {
    this.makeBuses;
    this.makeGroups;
    this.makeRoute;
    this.makeFX;
    this.makeTrigs;
}

makeBuses {
    server = Server.default;
    revBus = Bus.audio(server, numSpeakers);
    aBus = Bus.audio(server, numSpeakers);
}

makeGroups {
    group = Group.head(targetGroup);
    kGroup = Group.head(group);
    instrGroup = Group.after(kGroup);
    routeGroup = Group.after(instrGroup);
    fxGroup = Group.after(routeGroup);
    outGroup = Group.after(fxGroup);
}

makeRoute {
    route = [
        Synth(\route, [\in, aBus, \out, out, \amp, 1], instrGroup),
        Synth(\route, [\in, aBus, \out, revBus, \amp, 0], instrGroup), 
    ];
}

makeFX {
    reverb = Synth(\jpverb, [\revtime, revTime, \in, revBus, \out, out], fxGroup);
}

makeTrigs {
    trigs = minibeeID.collect{|id| 
        MBDeltaTrig.new( 
            speedlim: speedlim, 
            threshold: threshold, 
            minibeeID: id, 
            minAmp: minAmp, 
            maxAmp: maxAmp,
            function: { |dt, minAmp, maxAmp|
                var buf = bufs.choose;
                var dur = (buf.duration * durScale).clip(0, maxDuration);
                var speed_ = dur * speed;
                var pos = [0, 1].rotate([0, 1].wchoose([1-reverseProbability, reverseProbability])); 
                var attack = dur * attackScale;
                var release = dur * releaseScale;
                var revLevel= rrand(revLevelRange[0], revLevelRange[1]);
                var phasorBus = Bus.control(server, 1);
                var phasor = Synth(\kline, [
                    \dur, dur, 
                    \out, phasorBus,
                    \start, pos[0],
                    \end, pos[1],
                ], kGroup);
                var grFreqBus = Bus.control(server, 1);
                var grFreq = Synth(\kline, [
                    \dur, dur, 
                    \start, rrand(grainFreqRange[0], grainFreqRange[1]), 
                    \end, rrand(grainFreqRange[0], grainFreqRange[1]), 
                    \out, grFreqBus
                ], kGroup);
                var panBus = Bus.control(server, 1);
                var pan = Synth(\kline, [
                    \dur, dur, 
                    \start, rrand(panRange[0], panRange[1]), 
                    \end, rrand(panRange[0], panRange[1]), 
                    \out, panBus,
                    \dur, speed_,
                ], kGroup);
                var impulseBus = Bus.control(server, 1);
                var impulse = Synth(\impulse, [
                    \freq, grFreqBus.asMap, 
                    \out, impulseBus
                ], kGroup); 
                var eqBus = Bus.audio(server, numSpeakers);
                var eq = Synth(\eq, [
                    \in, eqBus, 
                    \locut, locut, 
                    \hishelffreq, hishelffreq, 
                    \hishelfdb, hishelfdb,
                    \hicut, hicut,
                    \peakfreq, peakfreq,
                    \peakrq, peakrq,
                    \peakdb, peakdb,
                    \out, aBus,
                ], 
                target: instrGroup); 
                var rate = exprand(rateRange[0], rateRange[1]);
                var grainSize = exprand(grainSizeRange[0], grainSizeRange[1]);
                var synth = Synth(\grbuf, 
                    [
                        \buf, buf,
                        \amp, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                        \attack, attack,
                        \pos, phasorBus.asMap,
                        \rate, rate,
                        \grainsize, grainSize,
                        \trig, impulseBus.asMap,
                        \rel, release,
                        \pan, panBus.asMap,
                        \out, eqBus, 
                    ], 
                    target: instrGroup,
                );
                SystemClock.sched((dur - attack - release), {synth.release(release)});
                SystemClock.sched(dur, { [grFreqBus, grFreq, panBus, pan, phasorBus, impulse, impulseBus, eq, eqBus].do(_.free); });
                reverb.set(\revtime, revTime);
                route[1].set(\amp, revLevel);
            };
        );
    }
}

play {
    trigs.do(_.play);
}

free { arg release = 5;
    TaskProxy {
        trigs.do(_.free);
        instrGroup.release(release);
        reverb.release(release);
        route.do(_.release(release));
        release.wait;
        aBus.free;
        revBus.free;
        out.free;
        group.free;
    }.play;
}
} 
