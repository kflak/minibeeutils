MBEnvTrig {
    var <minibeeID, <speedlim, <threshold, <minAmp, <maxAmp, <attack, <release, <>bus, <>group, <>addAction;
    var <>env, trig, <>gate=0, <>server;

    *new { | minibeeID=10, speedlim=0.5, threshold=0.1, minAmp=0.0, maxAmp=0.6, attack=1, release=4, bus, group, addAction=\addToHead |
        ^super.newCopyArgs(minibeeID, speedlim, threshold, minAmp, maxAmp, attack, release, bus, group, addAction).init;
    }

    init{
        server = server ? Server.default;
        bus = Bus.control(server, 1);
        this.envGen;
        this.makeTrig;
    }

    minibeeID_ {|i|
        minibeeID = i;
        trig.minibeeID = 1;
    }

    speedlim_ {|i|
        speedlim = i;
        trig.speedlim = speedlim;
    }

    threshold_ {|i|
        threshold = i;
        trig.threshold = threshold;
    }
    
    minAmp_ {|i|
        minAmp = i;
        trig.minAmp = minAmp;
    }

    maxAmp_ {|i|
        maxAmp = i;
        trig.maxAmp = i;
    }

    attack_ {|i|
        attack = i;
        env.set(\attack, attack);
    }

    release_ {|i|
        release = i;
        env.set(\release, release);
    }

    envGen {
        env = Synth(\env, [
            \attack, attack,
            \release, release,
            \amp, 0,
            \gate, 0,
            \da, 0,
            \out, bus
        ], group, addAction: addAction)
    }

    makeTrig {
        trig = MBDeltaTrig.new( 
            speedlim: speedlim, 
            threshold: threshold, 
            minibeeID: minibeeID, 
            function: { |dt|
                fork{
                    env.set(
                        \gate, 1, 
                        \amp, dt.linlin(0, 1, minAmp, maxAmp),
                    );
                    speedlim.wait;
                    env.set(\gate, 0);
                }
            };
        );
    }

    play {
        trig.play;
    }

    free {
        fork{
            trig.stop;
            env.set(\gate, 0); 
            release.wait;
            env.free;
            bus.free; 
        }
    }
} 
