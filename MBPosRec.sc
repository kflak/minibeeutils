MBPosRec {
    classvar <>data;
    classvar <>resamplingFreq = 20;

    var <>minibeeIDs, <>speedlim, <>threshold, <>action, <recArray, <currentPos, task, free=true;

    *new { | minibeeIDs, speedlim=1, threshold=0.1, action |
        ^super.newCopyArgs(minibeeIDs, speedlim, threshold, action).init;
    }

    init {
        recArray = List.newClear(16);
    }

    getCurrentPos {
        var tmp = nil ! minibeeIDs.size;
        minibeeIDs.do{|id, idx|
            tmp[idx] = [
                data[id].x,
                data[id].y,
            ];
        };
        currentPos = tmp.flat.copy;
    }

    rec {|idx=0|
        this.getCurrentPos;
        recArray.put(idx, currentPos.copy);
    }

    createTask {
        task = TaskProxy{
            inf.do{
                this.getCurrentPos;
                if(free){ this.compare };
                resamplingFreq.reciprocal.wait;
            }
        }
    }

    compare {
            recArray.do{|pos, idx|
                var diff;
                if(pos.notNil){
                    diff = (pos - currentPos).abs.sum;
                    diff = diff / minibeeIDs.size;
                    if (diff < threshold){ 
                        action.value(idx);
                        free = false;
                        SystemClock.sched(speedlim, {
                            free = true;
                        })
                    }
                }
            } 
    }

    play {
        this.createTask;
        task.play;
    }

    stop {
        task.stop;
    }

    free {
        task.clear;
    }
}
