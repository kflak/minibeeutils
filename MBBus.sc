MBBus {
    classvar <>resamplingFreq = 20;
    classvar <>mbs = #[9, 10, 11, 12, 13, 14, 15, 16];
    classvar <>mbData;

    var <>minibeeID;
    var <param;
    var <>minValue;
    var <>maxValue;
    var <>curve;
    var <bus;
    var <>server;
    var task;

    *new {|minibeeID=9, param=\x, minValue=0.0, maxValue=1.0, curve=0|
        ^super.newCopyArgs(minibeeID, param, minValue, maxValue, curve).init
    }

    init {
        server = server ? Server.default;
        this.prCreateTask;
    }

    prCreateTask {
        bus = bus ?? {Bus.control(server, 1)};
        task = TaskProxy.new({
            var in, val;
            inf.do {
                switch(param, 
                    nil, { in = mbData[minibeeID].x },
                    \x, { in = mbData[minibeeID].x },
                    \y, { in = mbData[minibeeID].y },
                    \z, { in = mbData[minibeeID].z },
                    \delta, { in = mbData[minibeeID].delta }
                );
                if(val.notNil){
                    val = in.lincurve(0.0, 1.0, minValue, maxValue, curve);
                    bus.set(val);
                };
                resamplingFreq.reciprocal.wait;
            }
        }).play;
        task.player.addDependant({|object, what, val|
            if(what==\stopped){
                "stopped - freeing the bus".postln;
                bus.free;
            }
        });
    } 

    free {
        task.stop;
    }
}
