MBScene {
    var <group;
    var <>fx;
    var <route;
    
    *new { arg fx;
        ^super.newCopyArgs(fx).init;
    }

    init {
        this.createGroups;
        this.createFx;
    }

    createGroups {
        group = Dictionary.new();
        group[\main] = Group.new;
        group[\k] = Group.after(group[\main]);
        group[\instr] = Group.after(group[\k]);
        group[\route] = Group.after(group[\instr]);
        group[\fx] = Group.after(group[\route]);
        group[\out] = Group.after(group[\fx]);
    }

    createFx {
        var fxTmp;
        if (fx.notNil){ fx.do(_.free) };
        fxTmp = fx;
    }

    routeSynths {
    }


    free {
        group.do(_.free);
    }

}
